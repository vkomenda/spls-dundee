all: build

build: site
	./site build

site: site.hs
	ghc --make site.hs
	./site clean

preview: site
	./site preview

clean:
	if [ -f site ]; then site clean; rm site; fi
	rm *.hi
	rm *.o
