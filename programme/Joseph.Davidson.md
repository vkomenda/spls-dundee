---
author: Joseph Davidson (Heriot-Watt)
title: Models and Modalities
time: 15:00-15:30
---

In this talk I will attempt to examine the relationship between the size of a
program written in a particular language and the expressiveness of the semantics
of that language. We compare across the program/memory boundary using Random
Access Stored Program (RASP) machines as a Von Neumann model, and the classic
Turing Machine as our Harvard architecture. But will also iterate on our RASP
machine to see how minor semantic changes affect our program sizes within a
single memory model.

In addition, I explain how semantics are not as suitable as we’d like for these
comparisons and use this to motivate how I have grounded our models using Field
Programmable Gate Arrays. I’ll will present the current results from both the
program+semantics analysis and grounding into FPGAs.