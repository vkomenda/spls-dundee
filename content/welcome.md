---
title: Scottish Programming Languages Seminar 2014 Winter Meeting
---

Welcome to the site of the SLPS 2014 winter meeting, on **Wednesday 26th of
February**, hosted by the [School of Computing, University of
Dundee](http://www.computing.dundee.ac.uk/). Past SPLS events are available on
the [SPLS webpage](http://www.dcs.gla.ac.uk/research/spls/).

**If you intend to attend, please [register
here](http://www.doodle.com/q2gie7n7g8vxpk3r).**

The invited talk will be given by [**Andy
Gordon**](http://research.microsoft.com/~adg/) (Microsoft Research and
University of Edinburgh).

Below is the up to date programme.  A [detailed programme](/programme.html) is
also available.

## Programme
