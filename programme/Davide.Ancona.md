---
author: Davide Ancona (University of Genoa)
title: Sound and complete subtyping between coinductive types for
       object-oriented languages
time: 14:30-15:00
---

In object-oriented programming, recursive types typically correspond to
inductively defined data structures; however, objects can also be cyclic, and
inductive types cannot represent them so precisely as happens for coinductive
types.  We study semantic subtyping between coinductive types with records and
unions, which are particularly interesting for object-oriented programming, show
cases where it allows more precise type analysis, and develop a sound and
complete effective algorithm for deciding it.