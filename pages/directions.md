---
title: Getting here
---

## Google map

<iframe width="800" height="350" frameborder="0" scrolling="no" marginheight="0"
marginwidth="0"
src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=200054289905990542132.0004d0bbf9b6d589fc6de&amp;ie=UTF8&amp;t=m&amp;ll=56.457653,-2.97678&amp;spn=0.016599,0.036478&amp;z=14&amp;output=embed">
</iframe>

## Travel by rail

There are direct services to Dundee from Newcastle, York and London on the East
Coast main line and from Carlisle, Preston, Birmingham, Coventry, Oxford,
Bristol, Plymouth, Reading, Southampton, Bournemouth on the West Coast main
line. There are overnight sleeper services to and from London. Rail journeys
from the other major cities in Scotland (Aberdeen, Edinburgh, Glasgow) take
approximately 75 minutes and the regular service from London King's Cross takes
only six hours. The University is only a few minutes walk from the railway
station. Information on train times can be found from Network Rail or by phoning
the National Rail Enquiries Centre on 08457 48 49 50.

## Travel by air

The University is only five minutes drive from Dundee airport. Scheduled flights
to and from London City airport connect with many other destinations within the
UK, Europe and worldwide. For more information on flights to Dundee airport from
London City contact Air France. Scheduled flights are also available to and from
Birmingham airport and Belfast City airport. For more information contact
FlyBe. For any further information phone Dundee Airport on 01382 643242.

Alternatively, Edinburgh international airport can be reached by motorway in
approximately one hour.

## Travel by car

Dundee is linked to the rest of the UK by continuous motorway and dual
carriageway. The main road linking Dundee is the A90, on approach to Dundee,
follow signs for City Centre and University. Car parking on campus is by permit
only, visitors should contact the School of Computing prior to travel to arrange
visitor permits, or use the public car park to the north of building 14.
