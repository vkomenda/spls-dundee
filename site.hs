{-# LANGUAGE OverloadedStrings, TupleSections #-}

import Hakyll

import Control.Monad       ( liftM )
import Data.List           ( sortBy )
import qualified Data.Map as Map
import Data.Monoid         ( (<>) )
import Data.Ord            ( comparing )
import System.FilePath     ( takeBaseName )

main :: IO ()
main = hakyll $ do
  match "images/*" $ do
    route   idRoute
    compile copyFileCompiler

  match "css/*" $ do
    route   idRoute
    compile compressCssCompiler

  match "pages/*" $ do
    route (setExtension "html" `composeRoutes`
           gsubRoute "pages/" (const ""))
    compile $ pandocCompiler
          >>= loadAndApplyTemplate "templates/default.html" defaultContext
          >>= relativizeUrls

  match ("content/*") $
    compile pandocCompiler

  match "programme/*" $ do
    compile pandocCompiler

  create ["programme.html"] $ do
    route idRoute
    compile $ do
      programme <- sortByTime =<< loadAll "programme/*"
      let archiveCtx =
            listField "programme" defaultContext (return programme) <>
            constField "title" "Programme" <>
            defaultContext
      makeItem ""
        >>= loadAndApplyTemplate "templates/programme.html" archiveCtx
        >>= loadAndApplyTemplate "templates/default.html" archiveCtx
        >>= relativizeUrls

  create ["index.html"] $ do
    route idRoute
    compile $ do
      programme <- sortByTime =<< loadAll "programme/*"
      contentCtx <- getContentCtx
      let indexCtx =
            titleCtx "content/welcome.md" <> contentCtx <>
            listField "programme" defaultContext (return programme)
      makeItem ""
        >>= loadAndApplyTemplate "templates/index.html" indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls

  match "templates/*" $ compile templateCompiler


getContentCtx :: Compiler (Context String)
getContentCtx =
  return . foldr ((<>) . ctx) defaultContext =<< loadAll "content/*"
  where
    ctx (Item ident body) =
      constField (takeBaseName (toFilePath ident)) body

titleCtx :: Identifier -> Context String
titleCtx ident = field "title" $ \_ -> do
  metadata <- getMetadata ident
  return $ Map.findWithDefault "Untitled" "title" metadata

sortByTime :: MonadMetadata m => [Item a] -> m [Item a]
sortByTime = sortByM getItemTime

sortByM :: (Monad m, Ord k) => (a -> m k) -> [a] -> m [a]
sortByM f =
  liftM (map fst . sortBy (comparing snd)) .
  mapM (\x -> liftM (x,) (f x))

getItemTime :: MonadMetadata m => Item a -> m String
getItemTime item = do
  metadata <- getMetadata $ itemIdentifier item
  return $ Map.findWithDefault "undefined time" "time" metadata
