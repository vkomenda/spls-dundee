---
title: Restricted dependent bounded linear types for arbitrary resources
author: Fredrik Nordvall Forsberg (Strathclyde)
time: 17:00-17:30
---

I present a type system for resource allocation, parameterised by an arbitrary
notion of resource as in Ghica and Smith's "Bounded Linear Types" and Brunel,
Gaboardi, Mazza, and Zdancewic's "Coeffect Calculus". Compared to loc. cit.,
this system includes a restricted notion of data dependency implemented by
indexing type judgements over simple index and resource languages, which is a
simplification of Dal Lago and Gaboardi's "Linear Dependent Types". The
motivation of this work is to provide a fairly precise resource analysis (for
arbitrary resources) in the type system while allowing a simple type-inference
algorithm. In this talk I justify the design decisions behind the type system
and illustrate it with simple examples (and non-examples). The main technical
contribution is a coherent categorical semantics using indexed categories. (This
is joint work with Dan Ghica.)
