Source code for the *SPLS 2014 in Dundee* website
=================================================

The website is written in Haskell using the
[Hakyll](http://jaspervdj.be/hakyll/) package, with HTML templates and Markdown
`.md` content files. The latter fulfil several purposes:

- quick content updates without the need to change Haskell or HTML code,
- batch processing,
- indexing programme items with keys such as `title` or `author`,
- automated sorting of items.

The latter three are all done in Haskell automatically.


Maintenance
-----------

Site maintenance is simple. To add a new programme item, go to `programme/` and
add a new `.md` file using the intuitive syntax that can be easily grasped by
looking at examples provided there.

To compile, run `make all`. To preview the compiled site, either

- load `_site/index.html` in the browser or
- run `make preview` and browse `localhost:8000` if that worked.

Before subsequent compilations you may need to clean up temporary files, for
which you can run `make clean`.

Site publishing is not automated at the moment. The compiled site is located in
`_site/`. The contents of that directory should be placed in the desired
location on the web server.


*Vladimir Komendantskiy*