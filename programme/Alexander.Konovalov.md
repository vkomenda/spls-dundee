---
author: Alexander Konovalov (St Andrews)
title: Recomputation in Scientific Experiments
time: 15:30-16:00
---

Reproducibility of scientific experiments is crucial for science.
However, computational experiments are rarely replicated, and even
when the source code has been made available, this may be still
far away from running it to replicate the same results.

We claim that it should be as easy to reproduce a computational
experiment as to reproduce the chemical reaction from a textbook by
mixing baking soda and lemon juice in your kitchen. I will present
the project Recomputation.org which suggests how we could make
this happen.
