---
title: Using Erlang Skeletons to Parallelise Realistic Medium-Scale Parallel Programs
author: Adam Barwell (St Andrews)
time: 16:30-17:00
---

With multi-core processors now standard, the performance boost that came with
each single-core generation is no longer free. Parallelism allows a program to
take full advantage of the hardware upon which it is run, and so achieve
performance gains. Yet despite the dramatic shift in hardware, parallelism
techniques have not kept pace. Current techniques are often ad hoc, featuring
low-level libraries that require expert knowledge of locks, synchronisation,
communication, etc. As the average programmer is not expert in parallelism,
parallel programming remains difficult and error-prone. What is needed is a
means to help programmers think parallel.

Erlang provides some reprieve, featuring a concurrency model that handles
lightweight processes, load balancing, and simple message passing. With a
scalable concurrency model, and higher-order functions affording flexible
abstraction, Erlang is well suited to embrace parallelism. Yet Erlang's
primitives are still low-level, requiring the programmer to manually manage
processes and message passing.

An algorithmic skeleton is an abstract computational entity that models some
common pattern of parallelism. Skeletons are typically implemented as high-level
functions, abstracting away low-level operations. Skel is an Erlang DSL that
provides fundamental and classical skeletons that are easily introduced,
replaced, and nested.

In this talk, we present three medium-scale use cases: the Discrete Haar Wavelet
Transform, the Single Total Weighted Tardiness Problem using Ant Colony
Optimisation, and an image merge. Using Skel, and a 24 core shared memory
machine, we parallelise these use cases, demonstrating that with relatively
little effort the Skel library can be used to simply parallelise Erlang
applications and obtain good speedups.
